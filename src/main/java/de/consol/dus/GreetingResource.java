package de.consol.dus;

import io.quarkus.security.Authenticated;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;

@Path("/hello")
@SecurityRequirement(name = "Keycloak")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public String hello() {
        return "Hello RESTEasy";
    }
}